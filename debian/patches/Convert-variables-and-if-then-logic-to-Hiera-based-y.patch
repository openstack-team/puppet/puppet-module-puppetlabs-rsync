Description: Convert variables and if/then logic to Hiera-based yaml files.
Author: Brian Schonecker <brian.schonecker@nfiindustries.com>
Date: Wed, 13 Jul 2022 14:16:25 -0400
Origin: upstream, https://github.com/puppetlabs/puppetlabs-rsync/commit/b533cc653cc4a1326fdb90f0debd37a546759df2.patch
Last-Update: 2024-02-15

diff --git a/data/os/Debian.yaml b/data/os/Debian.yaml
new file mode 100644
index 0000000..96b9ef3
--- /dev/null
+++ b/data/os/Debian.yaml
@@ -0,0 +1,3 @@
+---
+
+rsync::server::conf_file: '/etc/rsyncd.conf'
diff --git a/data/os/FreeBSD.yaml b/data/os/FreeBSD.yaml
new file mode 100644
index 0000000..071ce09
--- /dev/null
+++ b/data/os/FreeBSD.yaml
@@ -0,0 +1,4 @@
+---
+
+rsync::server::conf_file: '/usr/local/etc/rsync/rsyncd.conf'
+rsync::server::servicename: 'rsyncd'
diff --git a/data/os/RedHat.yaml b/data/os/RedHat.yaml
new file mode 100644
index 0000000..fa28c7f
--- /dev/null
+++ b/data/os/RedHat.yaml
@@ -0,0 +1,4 @@
+---
+
+rsync::server::conf_file: '/etc/rsyncd.conf'
+rsync::server::servicename: 'rsyncd'
diff --git a/data/os/RedHat/8.yaml b/data/os/RedHat/8.yaml
new file mode 100644
index 0000000..3ef475e
--- /dev/null
+++ b/data/os/RedHat/8.yaml
@@ -0,0 +1,4 @@
+---
+
+# RHEL8 and newer have a separate package for the rsync daemon.
+rsync::server::package_name: rsync-daemon
diff --git a/data/os/RedHat/9.yaml b/data/os/RedHat/9.yaml
new file mode 100644
index 0000000..3ef475e
--- /dev/null
+++ b/data/os/RedHat/9.yaml
@@ -0,0 +1,4 @@
+---
+
+# RHEL8 and newer have a separate package for the rsync daemon.
+rsync::server::package_name: rsync-daemon
diff --git a/data/os/Suse.yaml b/data/os/Suse.yaml
new file mode 100644
index 0000000..fa28c7f
--- /dev/null
+++ b/data/os/Suse.yaml
@@ -0,0 +1,4 @@
+---
+
+rsync::server::conf_file: '/etc/rsyncd.conf'
+rsync::server::servicename: 'rsyncd'
diff --git a/manifests/server.pp b/manifests/server.pp
index 4c155ff..ca43fa7 100644
--- a/manifests/server.pp
+++ b/manifests/server.pp
@@ -7,39 +7,22 @@
 #   class rsync
 #
 class rsync::server(
-  $use_xinetd = true,
-  $address    = '0.0.0.0',
-  $motd_file  = 'UNSET',
+  Boolean                                    $use_xinetd = true,
+  $address                                               = '0.0.0.0',
+  $motd_file                                             = 'UNSET',
   Variant[Enum['UNSET'], Stdlib::Absolutepath] $pid_file = '/var/run/rsyncd.pid',
-  $use_chroot = 'yes',
-  $uid        = 'nobody',
-  $gid        = 'nobody',
-  $modules    = {},
+  $use_chroot                                            = 'yes',
+  $uid                                                   = 'nobody',
+  $gid                                                   = 'nobody',
+  $modules                                               = {},
+  Optional[String[1]]                      $package_name = undef,
+  String[1]                                   $conf_file = '/etc/rsync.conf',
+  String[1]                                 $servicename = 'rsync',
+  Stdlib::Ensure::Service                $service_ensure = 'running',
+  Variant[Boolean, Enum['mask']]         $service_enable = true,
+  Boolean                                $manage_package = $rsync::manage_package,
 ) inherits rsync {
 
-  case $facts['os']['family'] {
-    'Debian': {
-      $conf_file = '/etc/rsyncd.conf'
-      $servicename = 'rsync'
-    }
-    'Suse': {
-      $conf_file = '/etc/rsyncd.conf'
-      $servicename = 'rsyncd'
-    }
-    'RedHat': {
-      $conf_file = '/etc/rsyncd.conf'
-      $servicename = 'rsyncd'
-    }
-    'FreeBSD': {
-      $conf_file = '/usr/local/etc/rsync/rsyncd.conf'
-      $servicename = 'rsyncd'
-    }
-    default: {
-      $conf_file = '/etc/rsync.conf'
-      $servicename = 'rsync'
-    }
-  }
-
   if $use_xinetd {
     include xinetd
     xinetd::service { 'rsync':
@@ -50,18 +33,23 @@ class rsync::server(
       require     => Package['rsync'],
     }
   } else {
-    if ($facts['os']['family'] == 'RedHat') and
-        (Integer($facts['os']['release']['major']) >= 8) and
-        ($rsync::manage_package) {
-      package { 'rsync-daemon':
-        ensure => $rsync::package_ensure,
-        notify => Service[$servicename],
+
+    # Manage the installation of the rsyncd package?
+    if $manage_package {
+
+      # RHEL8 and newer (and their variants) have a separate package for rsyncd daemon.  If the $package_name
+      # variable is defined (the variable is defined in the hiera hierarchy), then install the package.
+      if $package_name {
+        package {$package_name:
+          ensure => $rsync::package_ensure,
+          notify => Service[$servicename],
+        }
       }
     }
 
     service { $servicename:
-      ensure     => running,
-      enable     => true,
+      ensure     => $service_ensure,
+      enable     => $service_enable,
       hasstatus  => true,
       hasrestart => true,
       subscribe  => Concat[$conf_file],
-- 
2.39.2

